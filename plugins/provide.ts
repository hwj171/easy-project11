// 编写依赖注入提供者
export default defineNuxtPlugin(() => {
  const location = ref("Henan Shangqiu");
  function updateLocation(newLocation: string) {
    location.value = newLocation;
  }
  
  return {
    provide: {
      location: readonly({ location, updateLocation }),
    },
  };
});



// 使用
/* 
<template>
  <div>
    {{ $hello('world') }}
  </div>
</template>

<script setup lang="ts">
// alternatively, you can also use it here
const { $hello } = useNuxtApp()
</script>
 */
