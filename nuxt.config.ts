// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  css: ["@/assets/css/main.css"],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  vite: {
    test: {
      globals: true,
      environment: "happy-dom",
    },
  },
  modules: ['@element-plus/nuxt']
});
