import { UseFetchOptions } from "nuxt/dist/app/composables"

const fetch = (url: string, options: UseFetchOptions<any>) => {
  const { public: { env: { apiBaseURL } } } = useRuntimeConfig()
  const reqUrl = apiBaseURL + url

  return useFetch(reqUrl, { ...options })
}

const get = (url: string, options?: any) => {
  options['method'] = 'get'
  return fetch(url, options)
}

const post = (url: string, options?: any) => {
  options['method'] = 'post'
  return fetch(url, options)
}

const put = (url: string, options?: any) => {
  options['method'] = 'patch'
  return fetch(url, options)
}

const del = (url: string, options?: any) => {
  options['method'] = 'delete'
  return fetch(url, options)
}

const http = {
  get, post, put, del
}

export default http